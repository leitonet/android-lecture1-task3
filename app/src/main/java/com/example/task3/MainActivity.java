package com.example.task3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText email;
    EditText phone;
    EditText password;
    EditText rePassword;
    TextView textView;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.login);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);
        rePassword = (EditText) findViewById(R.id.re_password);

        textView = (TextView) findViewById(R.id.text_view);
        textView.setText(R.string.text_view);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if(login.getText().toString().isEmpty() || email.getText().toString().isEmpty() || phone.getText().toString().isEmpty() ||
                        password.getText().toString().isEmpty() || rePassword.getText().toString().isEmpty()){
                    textView.setText(R.string.empty_fields);
                }else {
                    if(password.getText().toString().equals(rePassword.getText().toString())) {
                        if(email.getText().toString().contains("@") && email.getText().toString().contains(".") && phone.getText().toString().startsWith("+38")){
                            textView.setText(R.string.validate_successful);
                        }else {
                            textView.setText(R.string.invalid_format);
                        }
                    }else {
                        textView.setText(R.string.different_passwords);
                    }
                }
            }
        });
    }
}
